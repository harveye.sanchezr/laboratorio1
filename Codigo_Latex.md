\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\graphicspath {  { ./images/ }  }
\usepackage{textcomp}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{ADQUISICION DE DATOS - STM/IMU\\

}

\author{\IEEEauthorblockN{1\textsuperscript{st} Cristian Camilo Gomez Trujillo}
\IEEEauthorblockA{\textit{Mecatronica} \\
\textit{Universidad ECCI}\\
Bogota, Colombia \\
cristianc.gomezt@ecci.edu.co}
\and
\IEEEauthorblockN{2\textsuperscript{nd} Harvey Estuard Sanchez Ramirez}
\IEEEauthorblockA{\textit{Mecatronica} \\
\textit{Universidad ECCI}\\
Bogota,Colombia \\
harveye.sanchezr@ecci.edu.co}
\and
\IEEEauthorblockN{3\textsuperscript{rd}Gabriel Castañeda Arias }
\IEEEauthorblockA{\textit{Mecatronica} \\
\textit{Universidad ECCI}\\
Bogota,Colombia \\
gabreil.castanedaa@ecci.edu.co}
\and

}

\maketitle

\begin{abstract}
- En este informe de laboratorio van a estar planteados algunos conceptos vistos en clase, se realizará una marco teórico sobre la IMU-MPU6050 y la tarjeta STM32F767zi con la comunicación I2C y serial; se va a exponer el análisis de resultados para concluir los logros de la práctica. Todo esto estará basado en el marco teórico y los resultados obtenidos.

\end{abstract}

\begin{IEEEkeywords}
comunicación I2C, IMU-MPU6050, STM32F767zi

\end{IEEEkeywords}
\vspace{0.5cm}
\section{Introducción}
Este informe va a contener diferentes secciones las cuales las iremos exponiendo durante el informe, nuestro tema para tratar en este informe será las diferentes habilidades aprendidas en clase para realizar principalmente la adquisición de 500 datos de la IMU con la STM32 a través de una comunicación I2C y serial para general los resultados en el monitor serial de Arduino. La programación está establecida en el software "EMBED" en donde se mostrará el Código con su respectiva explicación.
\\
\section{Marco teórico}

\subsection{Sensor IMU-MPU6050}

La unidad de procesamiento de movimiento MPU-60X0 es la primera solución del mundo en procesamiento de movimientos, con la fusión integrada de sensores con 9-ejes, la fusión de sensores utiliza su motor propiedad MotionFusion™ probado en el campo de teléfonos móviles, tabletas, aplicaciones, controladores de juegos, mandos a distancia, puntero de movimiento y otros dispositivos de consumo.El sensor MPU-6050 es una pequeña pieza tecnológica de procesamiento de movimiento. El cual mediante la combinación de un MEMS (Sistemas Micro electromecánicos) giroscopio de 3 ejes y un MEMS acelerómetro de 3 ejes en la misma pastilla de silicio junto con un DMP™ (Movimiento Digital Processor™), es capaz de procesar los algoritmos de movimientos complejos de 9 ejes (MotionFusion™) en una placa.[1]
\vspace{0.5cm}
\\
\begin{figure}[htb]
\centering
\includegraphics[width=6cm,height=5cm]{bibliography/mpu.jpg}
\caption{Sensor IMU-MPU6050}
\end{figure}
\vspace{0.5cm}
\subsection{Comunicación I2C}
I2C es un puerto y protocolo de comunicación serial, define la trama de datos y las conexiones físicas para transferir bits entre 2 dispositivos digitales. El puerto incluye dos cables de comunicación, SDA y SCL. Además el protocolo permite conectar hasta 127 dispositivos esclavos con esas dos líneas, con hasta velocidades de 100, 400 y 1000 kbits/s. El protocolo I2C es uno de los más utilizados para comunicarse con sensores digitales, ya que a diferencia del puerto Serial, su arquitectura permite tener una confirmación de los datos recibidos, dentro de la misma trama, entre otras ventajas.[2]
\begin{figure}[htb]
\centering
\includegraphics[width=8cm,height=6cm]{bibliography/i2c.png}
\caption{Comunicación i2c}
\end{figure}
\subsection{Mbed}
Es un lenguaje de programación para sistemas embebidos. Su principal ventaja es que puede compilarse en la nube, es decir desde Internet. Mbed se desarrolló pensando en el Internet de las Cosas. Además está enfocado a la programación de sistemas embebidos basados en arquitecturas ARM. Por ejemplo, existen más de 100 tarjetas de desarrollo que puedes elegir. Todas ellas con arquitectura ARM. El compilador que usa Mbed está en la” nube “. En otras palabras, se requiere de un explorador web como chrome, firefox o explorer para crear tus aplicaciones y programas.[3]
\begin{figure}[htb]
\centering
\includegraphics[width=8cm,height=8cm]{bibliography/mbed.png}
\caption{Plataforma MBED}
\end{figure}
\subsection{STM32F767zi}
La NUCLEO-F767ZI es una placa STM32 Núcleo-144 con microcontrolador STM32F767ZIT6. Proporciona a los usuarios una forma asequible y flexible de probar nuevos conceptos y desarrollar prototipos con el microcontrolador STM32, escogiendo distintas combinaciones de rendimiento, consumo de energía y funciones. El conector ST Zio, una extensión de Arduino Uno, proporciona acceso a más periféricos y los conectores ST Morpho facilitan la expansión de la funcionalidad de la plataforma de desarrollo abierta Núcleo con una amplia variedad de shields especializados. La placa STM32 Nucleo-144 no requiere una sonda independiente, ya que integra el depurador/programador ST-LINK/V2-1 e incluye una biblioteca HAL de software exhaustiva para STM32, así como varios ejemplos de paquetes de software y acceso directo a los recursos ARM mbed en línea. Es compatible con una amplia variedad de entornos de desarrollo integrado (IDEs) como IAR, Keil, IDEs basados en GCC y ARM mbed.
\begin{figure}[htb]
\centering
\includegraphics[width=8cm,height=8cm]{bibliography/stm.jpg}
\caption{Placa STM32F767zi}
\end{figure}

\section{PROCEDIMIENTO}
\subsection{Código}
Se realizo un programa por medio de la plataforma mbed para la adquisición de 500 datos del sensor IMU-mpu6050, estos fueron adquiridos y contienen la información de una variación mínima debido a que el sensor se encuentra en estado de reposo.
Los valores que suministra el sensor varían .

\\
\vspace{0.5cm}
\begin{figure}[htb]
\centering
\includegraphics[width=7cm,height=5cm]{bibliography/codigo1.jpg}
\caption{Código Parte 1}
\end{figure}

En esta parte del Código se definieron todas las constantes las cuales se obtuvieron de la ficha de datos de la IMU-MPU6050, el registro 0x68 se utiliza para restablecer las señales del giroscopio, el acelerómetro y los sensores de temperatura el cual se desplaza un bit hacia la izquierda para poder realizar la comunicación maestro - esclavo. Después están definidas las diferentes escalas del giroscopio y del acelerómetro, y se configuraron las diferentes sensibilidades de cada parámetro que mide la IMU. 

\\
\begin{figure}[htb]
\centering
\includegraphics[width=8cm,height=6.3cm]{bibliography/codigo2.JPG}
\caption{Código Parte 2}
\end{figure}

Luego se declararon la diferentes variables la cuales utilizaremos que son las de tipo interno las cuales tienes 16 bits, la de tipo double en las cuales se calculan las salidas calibradas, los bytes que se usan para escribir y leer los datos de la IMU por medio de la i2c y otro byte para almacenar los datos obtenidos, y demás variables.

\\
\begin{figure}[htb]
\centering
\includegraphics[width=7cm,height=0.8cm]{bibliography/codigo3.JPG}
\caption{Código Parte 3}
\end{figure}

Aquí se declaran los registros del puerto Tx y Rx, los cuales se ponen así por defecto para las stm32 si se utiliza por medio del cable usb, y el registro de la i2c con sus respectivos pines en este caso PB9 para el SDA y PB8 para el SCL.

\\
\begin{figure}[htb]
\centering
\includegraphics[width=9cm,height=8cm]{bibliography/codigo4.png}
\caption{Código Parte 4}
\end{figure}

En el int main inicialmente se escriben los registros 0x6B y 0x00 para despertar la IMU de ese estado de hibernación, luego se imprime un mensaje de inicio y se  escribe el registro para 0x75 para verificar si la conexión es correcta, la cual se indicara con un texto en el monitor serial. Cuando ya es verificada la correcta comunicación se escribe el registro 0x1B para configurar el giroscopio con un rango de 250 deg/s y el registro 0x1C para configurar el acelerómetro con un rango de 2g.

\\
\begin{figure}[htb]
\centering
\includegraphics[width=9cm,height=7cm]{bibliography/codigo5.png}
\caption{Código Parte 5}
\end{figure}

Dentro del while como bucle infinito espera recibir la letra "E" para iniciar la adquisición de 500 datos los cuales seran almacenados en cada una de las vaariables y calibradas segun su sensibilidad, y por ultimo se irán mostrando en el monitor serial aproximadamente cada 10ms.

\\

\subsection{Montaje}
Se ubica la IMU-MPU6050 en una protoboar y por medio de jumpers se hace el cableado a la stm32 donde se conecta el vcc a 5V en el pin 9 del CN8, el GND en el pin 11 del CN8, el SCl al PB8 en el pin 2 del CN7 y el SDA al PB9 en el pin 4 del CN7, y se conecta el cable de la stm al computador.

\vspace{0.5cm}
\begin{figure}[htb]
\centering
\includegraphics[angle=90,width=8cm,height=4cm]{bibliography/Montaje.jpeg}
\caption{Montaje}
\end{figure}
\subsection{Resultados}
Los resultados se muestran en el monitor serial de arduino, donde al presionar el reset de la stm se observar en el monitor un mensaje y la verifica de la correcta comunicación de la IMU mostrado en la figura 11, el enviar la letra "E" se empiezan a adquirí los datos de aceleración, del giroscopio, y del sensor de temperatura, aproximadamente cada 10 ms como podemos observar  se tiene una variación de +- 1 us, los datos de la aceleración en el eje X y Y se aproximan a cero y en el eje Z a 1g lo que nos dice que se encuentra en estado de reposo, los datos del giroscopio deberían de aproximarse a cero pero en el eje X y Y estas descalibrados y en el eje Z se encuentra mas estable, y por ultimo podemos observar los datos del sensor de temperatura. Al finalizar la adquisición de datos podemos observar en la figura 12 que se imprimieron 500 tramas de datos.  
\begin{figure}[htb]
\centering
\includegraphics[width=8.5cm]{bibliography/resultados1.png}
\caption{Resultados monitor serial 1}
\end{figure}

\begin{figure}[htb]
\centering
\includegraphics[width=8.5cm]{bibliography/resultados2.JPG}
\caption{Resultados monitor serial 2}
\end{figure}
\section*{Conclusiones}
\\
\subsubsection{}Se verifico que la comunicación I2c funciona correctamente debido a que la información suministrada por el sensor a la stm la podemos observar por medio de un puerto serial hacia un monitor serial del arduino.
\\
\subsubsection{}Al observar los resultados en el monitor serial nos damos cuenta que se están adquiriendo los datos correspondientes al acelerómetro, giroscopio y el sensor de temperatura.
\\
\subsubsection{}El análisis nos indica una leve variación en los datos , razón por la cual da constancia de que los componentes funcionan y el sensor es preciso en todos sus ejes de movimiento, excepto los ejes X y Y del giroscopio el cual necesito calibración.
\\
\subsubsection{}Concluyendo así se logro aplicar los conocimientos vistos en clase para realizar la practica y conocer tanto los componentes de los materiales como también su función y configuración.

\section*{REFERENCIAS}
[1] V.Garcia (2016) https://www.hispavila.com/el-sensor-mpu-6050/
\\

[2] Hetpro (2021) https://hetpro-store.com/TUTORIALES/i2c/
\\

[3] Alejndro Veloza Prieto (18/11/2018) https://os.mbed.com/users/alejandrosabio483/notebook/mbed-wiki/  
\\

[4] Farnell (20/06/2016) https://es.farnell.com/stmicroelectronics/nucleo-f767zi/placa-desarrollo-mcu-nucleo-32/dp/2546569

\end{document}
