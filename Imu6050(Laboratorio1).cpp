#include "mbed.h"

#define MPU6050_address 0xD0

#define GYRO_FULL_SCALE_250_DPS 0x00
#define GYRO_FULL_SCALE_500_DPS 0x08
#define GYRO_FULL_SCALE_1000_DPS 0x10
#define GYRO_FULL_SCALE_2000_DPS 0x18

#define ACC_FULL_SCALE_2_G 0x00
#define ACC_FULL_SCALE_4_G 0x08
#define ACC_FULL_SCALE_8_G 0x10
#define ACC_FULL_SCALE_16_G 0x18

#define SENSITIVITY_ACCEL 2.0/32768.0
#define SENSITIVITY_GYRO 250.0/32768.0
#define SENSITIVITY_TEMP 333.87

//Declaracion de variables
//Tipo interno
int16_t raw_accelx, raw_accely, raw_accelz;
int16_t raw_gyrox, raw_gyroy, raw_gyroz;
int16_t raw_temp;
//Tipo double "Salidas calibradas"
float accelx, accely, accelz;
float gyrox, gyroy, gyroz;
float temp;
//Bytes
char cmd[2];
char data[1];
char GirAcel[14];

float buffer[500][8];
int i;
Timer t;
float timer=0;

Serial pc(SERIAL_TX, SERIAL_RX);
I2C i2c(PB_9, PB_8);

int main(){
    cmd[0]=0x6B;
    cmd[1]=0x00;
    i2c.write(MPU6050_address, cmd, 2);
    
    pc.printf("Sensor acelerometro y giroscopio \n\r");
    
    pc.printf("Probando conexion \n\r");
    
    cmd[0]=0x75;
    i2c.write(MPU6050_address, cmd, 1);
    i2c.read(MPU6050_address, data, 1);
    if(data[0] != 0x98){
        pc.printf("Error de conexion \n\r");
        pc.printf("data: %#x \n\r",data[0]);
        pc.printf("\n\r");
        while(1);
    }else{
        pc.printf("Conexion establecida \n\r");
        pc.printf("\n\r");
    }
    wait(0.1);
    
    cmd[0] = 0x1B;
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);
    
    cmd[0] = 0x1C;
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);
    wait(0.01);

    while (true) {
        if(pc.getc() == 'E'){
            for(i=0;i<=499; i++){
                cmd[0] = 0x3B;
                i2c.write(MPU6050_address, cmd, 1);
                i2c.read(MPU6050_address, GirAcel, 14);
                t.reset();
                t.start();
                raw_accelx = GirAcel[0]<<8 | GirAcel[1];
                raw_accely = GirAcel[2]<<8 | GirAcel[3];
                raw_accelz = GirAcel[4]<<8 | GirAcel[5];
                raw_temp = GirAcel[6]<<8 | GirAcel[7];
                raw_gyrox = GirAcel[8]<<8 | GirAcel[9];
                raw_gyroy = GirAcel[10]<<8 | GirAcel[11];
                raw_gyroz = GirAcel[12]<<8 | GirAcel[13];
                
                accelx = raw_accelx*SENSITIVITY_ACCEL;
                accely = raw_accely*SENSITIVITY_ACCEL;
                accelz = raw_accelz*SENSITIVITY_ACCEL;
                gyrox = raw_gyrox*SENSITIVITY_GYRO;
                gyroy = raw_gyroy*SENSITIVITY_GYRO;
                gyroz = raw_gyroz*SENSITIVITY_GYRO;
                temp = (raw_temp/SENSITIVITY_TEMP)+21;
                wait_us(9997);
                t.stop();
                timer = t.read();
                pc.printf("El tiempo es %f segundos \r", timer);
                pc.printf("%d %.2f %.2f %.2f %.2f %.2f %.2f %.2f \n\r"
                ,i+1,accelx, accely, accelz, gyrox, gyroy, gyroz, temp);
            }
        }
    }
}